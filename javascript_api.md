# JavaScript - Appel d'API

Découvre comment appeler une API en JavaScript, avec fetch

## Ressources

- [HP-API](https://hp-api.onrender.com/api/characters)
- [Utiliser l'API Fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch)

## Contexte du projet

Une Application Programming Interface (**API**) fait la liaison entre deux programmes.

Prenons un exemple concret : l'application de messagerie *Signal* peut avoir besoin d'accéder aux contacts de ton téléphone. Le système d'exploitation de ton téléphone (`iOS` ou `Android`) possède une application native qui gère les contact. *Signal* et l'application de contact vont donc communiquer ensemble à l'aide d'une **API** !

Dans le domaine du *Web*, une **API** va souvent permettre à un site d'accèder aux données d'un autre site. Par exemple, imaginons que tu veuille faire un site sur l'univers de *Harry Potter* : tu pourrais accéder à la liste des personnages grâce à une **API** fournie par un autre site, [HP-API](https://hp-api.onrender.com/api/characters).

### JSON

Regardons à quoi ressemble le contenu de cette **API** :

```json
[
  {
    "id": "9e3f7ce4-b9a7-4244-b709-dae5c1f1d4a8",
    "name": "Harry Potter",
    "alternate_names": [
      "The Boy Who Lived",
      "The Chosen One"
    ],
    "species": "human",
    "gender": "male",
    "house": "Gryffindor",
    "dateOfBirth": "31-07-1980",
    "yearOfBirth": 1980,
    "wizard": true,
    "ancestry": "half-blood",
    "eyeColour": "green",
    "hairColour": "black",
    "wand": {
      "wood": "holly",
      "core": "phoenix feather",
      "length": 11
    },
    "patronus": "stag",
    "hogwartsStudent": true,
    "hogwartsStaff": false,
    "actor": "Daniel Radcliffe",
    "alternate_actors": [],
    "alive": true,
    "image": "https://ik.imagekit.io/hpapi/harry.jpg"
  },
  // {...},
  // {...},
  // {...},
]
```

Là normalement, tu devrais te dire "Ha, je sais ce que c'est !" et penser à un objet littéral en JavaScript (du moins, c'est ce que j'espère).

Et tu ne serais pas loin de la vérité : il s'agit d'un fichier **JSON** (acronyme de *JavaScript Object Notation*). C'est la représentation, au format texte, d'un objet.

Ici nous avons donc un tableau (on remarque les crochets) de personnages de l'univers de *Harry Potter*, chacun représenté par un objet (on remarque les accolades).

Un objet **JSON** peut contenir les types suivants :

- un tableau d'objet JSON
- un autre objet JSON
- une chaine de caractères
- un nombre
- un booléen
- *null*

> Un JSON n'autorisera pas les types *function*, *date* ou *undefined* !

Contrairement au JavaScript, les noms des propriétés (les clés) sont obligatoirement entre guillemets :

```json
// JSON
{
  "name": "Michel"
}
```

```js
// JavaScript
{
  name: "Michel"
}
```

### Fetch

Maintenant que tu sais ce qu'est une **API**, et le format **JSON**, nous allons voir comment les utiliser.

L'exemple va te montrer comment afficher la liste des noms des personnages de *Harry Potter* en JavaScript.

Pour se faire, il faut utiliser **l'API fetch**. Pour savoir comment appeler une **API** avec `fetch`, réfère-toi à cette documentation : [Utiliser l'API Fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch).

Une fois que tu auras lu la documentation ci-dessus, voici un exemple :

```js
async function getCharacters() {
  // appelle l'API et récupère la réponse
  const response = await fetch("https://hp-api.onrender.com/api/characters");
  // de la réponse, on garde uniquement le JSON, qui sera converti en JavaScript
  const characterList = await response.json();
}
```

Ne t'offusque pas de voir les mots `async` et `await` : tu découvriras plus tard à quoi ils servent exactement ! Pour le moment tu peux te dire que l'on demande à JavaScript **d'attendre** d'avoir la réponse de l'API pour continuer.

Si je veux afficher la liste des noms, je dois parcourir chaque élément du tableau d'objet, pour en récupérer la propriété "name" :

```js
async function getCharacters() {
  const response = await fetch("https://hp-api.onrender.com/api/characters");
  const characterList = await response.json();

  // pour chaque personnages du tableau
  for (const oneCharacter of characterList) {
    // on affiche le nom du personnage
    console.log(oneCharacter.name);
  }
}

// on appelle la fonction getCharacters
getCharacters();
/*
Harry Potter
Hermione Granger
Ron Weasley
...
*/
```

## Modalités pédagogiques

Ta mission est la suivante :

- Créé un script JavaScript permettant d'appeler l'API [https://jsonplaceholder.typicode.com/users](https://jsonplaceholder.typicode.com/users)
- Affiche la liste des utilisateurs récupérés par cette API, ainsi que leur adresse complète, de la forme : nom, rue, code postal, ville
- Ajoute des commentaires afin d'expliquer ton code
- Versionne ton code et partage un lien vers un dépôt GitLab en solution

## Modalités d'évaluation

- le code est hébergé sur GitLab
- le code est bien indenté, respecte les conventions de nommage et s'exécute sans erreur
- une liste des 10 noms d'utilisateurs ainsi que leur adresse apparaît, au format attendu

## Livrables

Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions