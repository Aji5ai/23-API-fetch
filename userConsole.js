async function getUsers() {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  const userList = await response.json();

  for (const user of userList) { // Pour chaque objet récupéré on affiche le nom et l'adresse
    console.log(
      `${user.name} habitant rue ${user.address.street}, de la ville ${user.address.city} (code postal : ${user.address.zipcode})`
    );
  }
}

getUsers(); // on appelle la fonction
