import "./App.css";
import React, { useEffect, useState } from "react";
import User from "./components/User.jsx";

function App() {
  // Déclare une variable d'état userList avec une fonction setUserList pour la mettre à jour
  const [userList, setUserList] = useState([]);

  // Utilise useEffect pour effectuer des opérations APRES le rendu initial du composant
  useEffect(() => {
    // Déclare une fonction asynchrone getUser pour récupérer les utilisateurs depuis l'API
    async function getUser() {
      // Effectue une requête fetch pour obtenir les utilisateurs depuis l'API
      const response = await fetch("https://jsonplaceholder.typicode.com/users");
      // Met à jour la variable d'état userList avec le JSON de la réponse
      setUserList(await response.json());
    }

    // Appelle la fonction getUser lors du rendu initial du composant
    getUser();
  }, []); // Le tableau vide [] signifie que cet effet s'exécute une seule fois après le rendu initial

  // Retourne le JSX du composant, affiche la liste des utilisateurs dans un élément <ul>
  return (
    <>
      <ul>
        {userList.map((currentElement) => (
          // Affiche le composant User pour chaque utilisateur dans la liste
          <User key={currentElement.id} {...currentElement} />
        ))}
      </ul>
    </>
  );
}

// Exporte le composant App pour qu'il puisse être utilisé ailleurs dans l'application
export default App;


// // Version avec try catch (non fonctionnelle)
// function App() {
//   async function getUsers() {
//     try {
//       const response = await fetch(
//         "https://jsonplaceholder.typicode.com/users"
//       );
//       if (!response.ok) {
//         throw new Error("La réponse n'est pas OK");
//       }
//       const userList = await response.json();
//       console.log(userList)
//       return (
//         <ul>
//           {userList.map((currentElement) => (
//             <User key={currentElement.id} {...currentElement} />
//           ))}
//         </ul>
//       );
//     } catch (error) {
//       console.error("Erreur :", error);
//     }
//   }
//   getUsers();
// }