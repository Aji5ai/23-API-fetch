export default function User(props) {
  const {name, address :{street, city, zipcode}} = props;
  // ou en + : const {street, city, zipcode} = address;
  return (
    <li>
      {name} from {street} {city} {zipcode}
    </li>
  );
}
