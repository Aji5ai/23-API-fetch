import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(<App />);
/* Strict mode dessine dans le dom (=rend) les éléments deux fois, donc on l'a enlevé pour que ça y soit qu'une fois*/